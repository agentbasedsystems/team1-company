package org.xploration.team1.company;


import org.xploration.ontology.*;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

public class Capsule1 extends Agent {
	
	private static final long serialVersionUID = 1L;
	
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private int mMissionLength; // number which represents seconds of mission length
    private int mRadioRange; // number which represents number of cells for radio range
    private Cell mCellPosition;
    private static ContentManager contentManager = new ContentManager();

	protected void setup()
	{
		contentManager.registerLanguage(codec);
		contentManager.registerOntology(ontology);
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
        
		System.out.println(getLocalName()+": Created by Spacecraft.");
		
		Object[] agentCapsuleArguments = (Object[]) getArguments();
		
		Integer x = (agentCapsuleArguments[0] != null) ? (Integer) agentCapsuleArguments[0] : -1;
		Integer y = (agentCapsuleArguments[1] != null) ? (Integer) agentCapsuleArguments[1] : -1;
		Integer N = (agentCapsuleArguments[2] != null) ? (Integer) agentCapsuleArguments[2] : -1;
		Integer M = (agentCapsuleArguments[3] != null) ? (Integer) agentCapsuleArguments[3] : -1;
		mMissionLength = (agentCapsuleArguments[4] != null) ? (Integer) agentCapsuleArguments[4] : -1;
		mRadioRange = (agentCapsuleArguments[5] != null) ? (Integer) agentCapsuleArguments[5] : -1;
		
		if (N == -1 || M == -1 || x == -1 || y == -1 || mMissionLength == -1 || mRadioRange == -1) {
			System.out.println(getLocalName()+": There is a problem with received arguments.");
		}
		
		System.out.println(getLocalName()+":" + "N=" + N + ", M=" + M + ", x=" + x + ", y=" + y + ", mission length = " + mMissionLength + ", radio range = " + mRadioRange);

		// create Cell
		mCellPosition = new Cell();
		mCellPosition.setX(x);
		mCellPosition.setY(y);
		
		addBehaviour(new CapsuleRegistration(this));
		addBehaviour(new ReceiveClaimCellsFromRover(this));
		
		instantiateRover(N, M, x, y);
		
		addBehaviour(missionLengthCountdown());
	}
	
	private void instantiateRover(int N, int M, int x, int y) {
		
		Integer[] agentRoverArguments = new Integer[6];
		agentRoverArguments[0] = x;
		agentRoverArguments[1] = y;
		agentRoverArguments[2] = N;
		agentRoverArguments[3] = M;
		agentRoverArguments[4] = mMissionLength;
		agentRoverArguments[5] = mRadioRange;
		
		AgentContainer mainContainer = getContainerController();
		
		try {
			AgentController agentController = mainContainer.createNewAgent("AgentRover1", "org.xploration.team1.company.AgentRover1", agentRoverArguments);
			agentController.start();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

	// ************************** //
	// *** BEHAVIOURS SECTION *** //
	// ************************** //
	
	// Claim cells (listen to claim cell inform from platform)
    class ReceiveClaimCellsFromRover extends CyclicBehaviour {

    	public ReceiveClaimCellsFromRover(Agent a) { 
            super(a);  
        }
        
    	private static final long serialVersionUID = 1L;
    	
    	@Override
		public void action() {
    		ACLMessage msg = receive(
					MessageTemplate.and(MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO),
					MessageTemplate.and(
					MessageTemplate.MatchLanguage(codec.getName()), 
					MessageTemplate.MatchOntology(ontology.getName()))));
    		if(msg != null){
				ContentElement ce = null;
				try
				{
					if (msg.getPerformative() == ACLMessage.INFORM) {
						ce = getContentManager().extractContent(msg);
						if(ce instanceof Action) {
	                        Action agAction = (Action) ce;
	                        Concept conc = agAction.getAction();
							if(conc instanceof ClaimCellInfo) {
								ClaimCellInfo cc = (ClaimCellInfo) conc;
								org.xploration.ontology.Map cellsToClaim = cc.getMap();
								int claimerId = cc.getTeam().getTeamId();
								System.out.println(myAgent.getLocalName()+": Received CLAIMCELLINFO from " + (msg.getSender()).getLocalName());
								
								//forward cell claim to spacecraft
								forwardCellClaimToSpaceCraft(claimerId, cellsToClaim);
								
							}
						}
					}

				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				// If no message arrives
				block();
			}  
		}
    	
    }
    
	// Claim cells (Capsule-Spacecraft) 
	private void forwardCellClaimToSpaceCraft(int claimerId, Map cellsToClaim)  {	
		
		addBehaviour(new SimpleBehaviour(this){
		
			private boolean mClaimCellSent = false;
			
			private static final long serialVersionUID = 1L;
			
			AID spacecraftAID = new AID();
	
			@Override
			public void action() {
				
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(XplorationOntology.SPACECRAFTCLAIMSERVICE);
				dfd.addServices(sd);
				
				try {
					DFAgentDescription[] res = new DFAgentDescription[1];
					res = DFService.search(myAgent, dfd);
	
					if (res.length > 0)
					{
						spacecraftAID = (AID) res[0].getName();
						// Send request for claim cell INFORM to spacecraft
						ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
						msg.addReceiver(spacecraftAID);
						msg.setLanguage(codec.getName());
						msg.setOntology(ontology.getName());
						
						msg.setProtocol(XplorationOntology.CLAIMCELLINFO);
						
						ClaimCellInfo claimCellInfo = new ClaimCellInfo();
						claimCellInfo.setMap(cellsToClaim);
						Team t = new Team();
						t.setTeamId(claimerId);
						claimCellInfo.setTeam(t);
						
						Action agAction = new Action(spacecraftAID, claimCellInfo);
						try
						{
							// The ContentManager transforms the java objects into strings
							getContentManager().fillContent(msg, agAction);
							send(msg);
							System.out.println(getLocalName()+": CLAIMCELLINFO sent to spacecraft!");
							mClaimCellSent = true;
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
	
			@Override
			public boolean done() 
			{
				return mClaimCellSent;
			}
				
		});
	}
	
	// Sends Capsule Registration Inform to Platform Simulator
	class CapsuleRegistration extends SimpleBehaviour {
	    	
    	public CapsuleRegistration(Agent a) { 
            super(a);  
        }
    	
    	boolean capsuleRegistrationSent = false;
		private static final long serialVersionUID = 1L;
		
		AID platformSimulator = new AID();

		@Override
		public void action() {
			
			// Creates the description for the type of agent to be searched
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType(XplorationOntology.CAPSULEREGISTRATIONSERVICE);
			dfd.addServices(sd);
			
			try {
				// It finds agents of the required type
				DFAgentDescription[] res = new DFAgentDescription[1];
				res = DFService.search(myAgent, dfd);
				// Gets the first occurrence, if there was success
				if (res.length > 0)
				{
					platformSimulator = (AID) res[0].getName();
					
					CapsuleRegistrationInfo capsuleRegistrationInfo = new CapsuleRegistrationInfo();
					
					capsuleRegistrationInfo.setCell(mCellPosition);
					Team myTeam = new Team();
					myTeam.setTeamId(CompanyConstants.MY_TEAM_ID);
					capsuleRegistrationInfo.setTeam(myTeam);
										
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.addReceiver(platformSimulator);
					msg.setLanguage(codec.getName());
					msg.setOntology(ontology.getName());
					
					msg.setProtocol(XplorationOntology.CAPSULEREGISTRATIONINFO);
					
					// As it is an action and the encoding language the SL, it must be wrapped into an Action
					Action agAction = new Action(platformSimulator, capsuleRegistrationInfo);
					try
					{
						// The ContentManager transforms the java objects into strings
						contentManager.fillContent(msg, agAction);
						send(msg);
						System.out.println(getLocalName()+": Capsule registration sent!");
						capsuleRegistrationSent = true;
					}
					catch (CodecException ce)
					{
						ce.printStackTrace();
					}

				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

		@Override
		public boolean done() 
		{
			return capsuleRegistrationSent;
		}
	}
	
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = mMissionLength * 1000;
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Terminate Agent Capsule
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
	
}
