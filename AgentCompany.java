package org.xploration.team1.company;

import org.xploration.ontology.*;

import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;

public class AgentCompany extends Agent {
	
	private static final long serialVersionUID = 1L;
	
	// Codec for the SL language used and instance of the ontology XplorationOntology that we have created
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    
    public final static int mTeamID = 1;
    private boolean registrationRequestSent = false;
	
	protected void setup()
	{
		System.out.println(getLocalName()+": Entered the system.");

		// Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        // BEHAVIOURS ****************************************************************
        addBehaviour(new SendCompanyRegistrationBehaviour(this));
        addBehaviour(new AwaitCompanyRegistrationResponseBehaviour(this));
	}
	
	// ************************** //
	// *** BEHAVIOURS SECTION *** //
	// ************************** //
	
	// This behaviour is sending a company registration request to Registration Desk Service of the AgentSpacecraft
    class SendCompanyRegistrationBehaviour extends SimpleBehaviour {

    	public SendCompanyRegistrationBehaviour(Agent a) { 
            super(a);  
        }
    	private static final long serialVersionUID = 1L;
		AID spacecraftAgentAID = new AID();
		
		@Override
		public void action() {
			
			// Creates the description for the type of agent to be searched
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType(XplorationOntology.REGISTRATIONDESK);
			dfd.addServices(sd);
			
			try {
				// We expect to have only one spacecraft registered
				DFAgentDescription[] res = new DFAgentDescription[1];
				res = DFService.search(myAgent, dfd);

				// Gets the first occurrence, if there was success
				if (res.length > 0) {
					spacecraftAgentAID = (AID) res[0].getName();
					// Send request for registration to the spacecraft
					ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
					msg.addReceiver(spacecraftAgentAID);
					msg.setLanguage(codec.getName());
					msg.setOntology(ontology.getName());
					
					msg.setProtocol(XplorationOntology.REGISTRATIONREQUEST);
					
					Team mTeam = new Team();
					mTeam.setTeamId(mTeamID);
				
					RegistrationRequest rr = new RegistrationRequest();
					rr.setTeam(mTeam);
					
					// As it is an action and the encoding language the SL, it must be wrapped into an Action
					Action agAction = new Action(spacecraftAgentAID, rr);
					try {
						// The ContentManager transforms the java objects into strings
						getContentManager().fillContent(msg, agAction);
						send(msg);
						System.out.println(getLocalName()+": REQUESTS REGISTRATION");
						registrationRequestSent = true;
					}
					catch (CodecException ce) {
						ce.printStackTrace();
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean done() {
			return registrationRequestSent;
		}
    	
    }
    // This behaviour is awaits for the response from the AgentSpacecraft about Company Registration Request
    class AwaitCompanyRegistrationResponseBehaviour extends CyclicBehaviour {

    	public AwaitCompanyRegistrationResponseBehaviour(Agent a) { 
            super(a);  
        }
    	// awaits for REFUSE, NOT UNDERSTOOD OR AGREE from AgentSpacecraft
		private static final long serialVersionUID = 1L;
		public void action()
		{
			
			ACLMessage msg = receive(MessageTemplate.and(MessageTemplate.MatchLanguage(codec.getName()), 
					MessageTemplate.MatchOntology(ontology.getName())));

			if (msg != null) {
				try {
					if (msg.getPerformative() == ACLMessage.REFUSE) {
						// If what is received is REFUSE
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": REFUSE --> Tried to register too late.");
					} else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
						// If what is received is NOT UNDERSTOOD
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " NOT UNDERSTOOD");
					} else if (msg.getPerformative() == ACLMessage.AGREE) {
						// If what is received is AGREE
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": AGREE");
					} else if (msg.getPerformative() == ACLMessage.INFORM) {
						// If what is received is INFORM SUCCESS
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": INFORM --> Successfuly registered!");
				        // Terminate after 5 seconds
						addBehaviour(missionLengthCountdown());
					} else if (msg.getPerformative() == ACLMessage.FAILURE) {
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": FAILURE");
						// Terminate after 5 seconds
						addBehaviour(missionLengthCountdown());
					} else {
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " received but it is not REFUSE, AGREE nor NOT UNDERSTOOD!");
					}

				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				// If no message arrives
				block();
			}
		}
    }
    
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = 5000; // Terminate after 5 seconds
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Terminate Agent Company
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
}
