package org.xploration.team1.company;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.xploration.ontology.*;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class AgentRover1 extends Agent {

	private static final long serialVersionUID = 1L;
	private static ContentManager contentManager = new ContentManager();
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    
    private Cell mRoverPosition;
    private Cell mCapsulePosition;
    private Cell mCellToMoveTo;
    // mMap is a 2-dim array of cells used to determine the next move.
    private Cell[][] mMap;
    
    // Queue of moves to be sent to the platform simulator
    private Queue<Cell> moves = new LinkedList<Cell>();
    
    // Flag to check if rover is moving to Capsule to claim cells
    // *This flag is used as a blocking variable so that adding movement to Capsule cell only occurs once!*
    private boolean movingToCapsuleToClaim = false;
    
    // Attributes needed by Rover Logic
    private enum RoverState {IDLE, MOVING, ANALYZING, FINISHED};
    private RoverState mRoverState;
    private Map mCellsToClaim;
    private Map mKnownCells;
    
    // Rover moving logic variables
    int maxNumberOfLevels;
    int currentLevel;
    // mMissionLength is a number that represents seconds of mission length
    private int mMissionLength;
    private int mRadioRange; // number which represents number of cells for radio range
    
    private int mNumberOfClaimedCells = 0;
    private int mNumberOfKnownCells = 0;
    
    private int mClaimBufferSize = 4;
    
    // Queue of moves to be sent to the platform simulator
    private ArrayDeque<Cell> mQueueOfCellsToMoveTo = new ArrayDeque<Cell>();
    
    private Integer N; // mMapHeight
    private Integer M; // mMapWidth
    
	protected void setup()
	{
		contentManager.registerLanguage(codec);
		contentManager.registerOntology(ontology);
		
		System.out.println(getLocalName()+": Created by Capsule1.");
		
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
		
		Team myTeamID = new Team();
		myTeamID.setTeamId(1);
		mCellsToClaim = new org.xploration.ontology.Map();
		mKnownCells = new org.xploration.ontology.Map();
		
		Object[] agentRoverArguments = (Object[]) getArguments();
		
		if (agentRoverArguments instanceof Integer[]) {
			// Checks for null
			Integer x = (agentRoverArguments[0] != null) ? (Integer) agentRoverArguments[0] : -1;
			Integer y = (agentRoverArguments[1] != null) ? (Integer) agentRoverArguments[1] : -1;
			N = (agentRoverArguments[2] != null) ? (Integer) agentRoverArguments[2] : -1;
			M = (agentRoverArguments[3] != null) ? (Integer) agentRoverArguments[3] : -1;
			mMissionLength = (agentRoverArguments[4] != null) ? (Integer) agentRoverArguments[4] : -1;
			mRadioRange = (agentRoverArguments[5] != null) ? (Integer) agentRoverArguments[5] : -1;
			
			if (N == -1 || M == -1 || x == -1 || y == -1 || mMissionLength == -1 || mRadioRange == -1) {
				System.out.println(getLocalName()+": There is a problem with received arguments.");
			}
			
			System.out.println(getLocalName()+":" + "N=" + N + ", M=" + M + ", x=" + x + ", y=" + y + ", mission length = " + mMissionLength + ", radio range = " + mRadioRange);
			
			// create a map of possible moves (size:height+4,width+4)
			mMap = generateMap(N,M);
			
			// This is initial position of our rover
			mRoverPosition = new Cell();
			mRoverPosition.setX(x);
			mRoverPosition.setY(y);
			
			// This is STATIC position of our capsule and it NEVER changes
			mCapsulePosition = new Cell();
			mCapsulePosition.setX(x);
			mCapsulePosition.setY(y);
			
			mCellToMoveTo = new Cell();
			
			mRoverState = RoverState.IDLE;
			
			addBehaviour(missionLengthCountdown());
			
			addBehaviour(claimBufferSize2());
			addBehaviour(claimBufferSize4());
			addBehaviour(claimBufferSize8());
			
			// Send rover registration inform to platform simulator
	        addBehaviour(new SendRoverRegistrationBehaviour(this));
	        
	        // add behaviours for rover movement (issue&wait)
	        addBehaviour(new IssueMovement(this));
	        addBehaviour(new WaitForMovementResponse(this));
	        
	        // maximum number of levels equals: bigger dimension / 2 - 1
	        maxNumberOfLevels = (N > M) ? N : M;
	        maxNumberOfLevels = maxNumberOfLevels / 2;
	        maxNumberOfLevels = maxNumberOfLevels - 1;
	        // end of calculations
	        currentLevel = 1;
	         
	        thinkRoverThink();
		}
	}

	private void thinkRoverThink() {
		
		// Rule #1: If did not analyze the current cell --> Analyze it!
		@SuppressWarnings("unchecked") // Get all the cells that are already known (analyzed or reported by other teams'rovers)
		Iterator<Cell> iterator = mKnownCells.getAllCellList();
		boolean alreadyAnalyzed = false;
		// If current cell is already known -> Don't analyze.
		while (iterator.hasNext()) {
			Cell cell = iterator.next();
			if (cell.getX() == mRoverPosition.getX() && cell.getY() == mRoverPosition.getY()) {
				alreadyAnalyzed = true;
			}
		}
		// If current cell is still not analyzed --> Analyze current cell!
		if (alreadyAnalyzed == false) { 
			mRoverState = RoverState.ANALYZING;
			cellAnalysisRequest(mRoverPosition);
		}
		
		// Rule #2: If in range of Capsule and has cells to claim in buffer --> Claim right away, as it does not produce any delay!
		if (isWithinRange(mRoverPosition, mCapsulePosition)) {
			// if there is something in buffer to claim
			if (mCellsToClaim.getCellList().size() != 0) {
				
				Map tmpCellsToClaim = new Map();
				for (int i=0; i<mCellsToClaim.getCellList().size(); i++) {
					Cell cellToAdd = new Cell();
					cellToAdd = (Cell) mCellsToClaim.getCellList().get(i);
					tmpCellsToClaim.addCellList(cellToAdd);
				}
				
				claimCells(tmpCellsToClaim);
				
				// This is just to keep count of how many cells where claimed
				mNumberOfClaimedCells = mNumberOfClaimedCells + mCellsToClaim.getCellList().size();
				System.out.println(getLocalName()+": Number of claimed cells = "+ mNumberOfClaimedCells);
				
				mCellsToClaim.clearAllCellList();
				movingToCapsuleToClaim = false;
			}
		}
		// Rule #3: Else if not in range but has mClaimBufferSize cells to claim in buffer --> Start moving within range of Capsule!
		else if ((mCellsToClaim.getCellList().size() >= mClaimBufferSize) && (movingToCapsuleToClaim==false)) {
			
			// Move within range of Capusle
			Cell firstCellWithinRange = new Cell();
			firstCellWithinRange = getFirstCellWithinRange(mRoverPosition, mCapsulePosition);	
			mQueueOfCellsToMoveTo.addFirst(firstCellWithinRange);
			
			// Move to Capsule
			//mQueueOfCellsToMoveTo.addFirst(mCapsulePosition);
			
			movingToCapsuleToClaim = true;
			System.out.println(getLocalName()+": Has " + mCellsToClaim.getCellList().size() +  "cells to claim but is too far from Capsule. Traveling to capsule to claim cells!");

		} 
		// Rule #4: If Rover's mQueueOfCellsToMoveTo is empty -> Add moves from the algorithm of moving in CIRCLES through LEVELS
		if (mQueueOfCellsToMoveTo.isEmpty()) {
			// if this if is not true, that means that the whole map is already analyzed and rover can just sit in place
			if (currentLevel <= maxNumberOfLevels) {
				ArrayList<Cell> circleOfCells = new ArrayList<Cell>();
				circleOfCells = getSurroundingCells(mCapsulePosition, currentLevel);
				mQueueOfCellsToMoveTo.addAll(circleOfCells);
				
				currentLevel++;
			}
			else {
				mRoverState = RoverState.FINISHED;
			}
		}
		
		// Rule #5: If rover state is IDLE (not moving or waiting for cell analysis to finish) --> Rover can move
		if (mRoverState == RoverState.IDLE) {
			// Ask to move to the first cell in the mQueueOfCellsToMoveTo
			Cell nextCellToMoveTo = new Cell();
			nextCellToMoveTo = mQueueOfCellsToMoveTo.poll(); // poll removes the head
			if (nextCellToMoveTo != null) {
				// Get cells that lead you to next cell to move to (maybe it is not adjacent)
				ArrayList<Cell> roadToNextCell = new ArrayList<Cell>();
				roadToNextCell = getListOfCellsToMove(mRoverPosition, nextCellToMoveTo);

				for (int i=0; i<roadToNextCell.size(); i++) {
					mQueueOfCellsToMoveTo.addFirst(roadToNextCell.get(i));
				}
				
				nextCellToMoveTo = mQueueOfCellsToMoveTo.poll();
				
				movementRequest(nextCellToMoveTo);
			}
		}
		// Rule #6: If rover state is FINISHED --> Rover has visited all the cells on this asteroid. He can rest.
		else if (mRoverState == RoverState.FINISHED) {
			// Stop the rover here. He knows all the cells.
			System.out.println(getLocalName()+": This rover has visited or knows about all the cells on this Asteroid. His job is done.");

			mNumberOfKnownCells = mKnownCells.getCellList().size();
			System.out.println(getLocalName()+": Number of known cells = " + mNumberOfKnownCells);
		}
	}

	// This method finds the shortest path to move between 2 cells.
	private Cell getFirstCellWithinRange(Cell source, Cell destination) {
		ArrayList<Cell> list = new ArrayList<Cell>();
		list = getListOfCellsToMove(source, destination);
		Cell cell = new Cell();
		
		// Loops through list and once it finds that it is in range of Capsule - > return that Cell
		for (int i=0; i<list.size(); i++) {
			cell = list.get(i);
			if (isWithinRange(cell, destination)) {
				return cell;
			}
		}
		return null; // if this happens -> Error
	}

	private boolean isEqualXY(Cell a, Cell b) {
		if(a.getX()==b.getX()&&a.getY()==b.getY()) return true;
		else return false;
	}
	
	private ArrayList<Cell> getListOfCellsToMove(Cell source, Cell destination) {
		ArrayList<Cell> list = new ArrayList<Cell>();
		//if adjacent, return the adjacent cell
		if(isEqualXY(moveUp(source),destination)) 			list.add(0, moveUp(source));
		else if(isEqualXY(moveTopLeft(source),destination)) 	list.add(0, moveTopLeft(source));
		else if(isEqualXY(moveBottomLeft(source),destination)) 	list.add(0, moveBottomLeft(source));
		else if(isEqualXY(moveDown(source),destination)) 		list.add(0, moveDown(source));
		else if(isEqualXY(moveBottomRight(source),destination)) list.add(0, moveBottomRight(source));
		else if(isEqualXY(moveTopRight(source),destination)) 	list.add(0, moveTopRight(source));
		if(list.isEmpty()) return moveToCell(list, source, destination);
		else return list;
	}
	
	// This method finds the shortest path to move between 2 cells.
	private ArrayList<Cell> moveToCell(ArrayList<Cell> list, Cell source, Cell destination){
		
		Cell toCell = new Cell();                                          
		if(source.getX()< destination.getX()&&	source.getY()< destination.getY()) 	toCell = moveBottomRight(source);
		else if(source.getX()< destination.getX()&&	source.getY()==destination.getY()) 	toCell = moveDown(source);
		else if(source.getX()< destination.getX()&&	source.getY()> destination.getY())	toCell = moveBottomLeft(source);
		else if(source.getX()==destination.getX()&&	source.getY()< destination.getY())	toCell = moveTopRight(source);
		else if(source.getX()==destination.getX()&&	source.getY()> destination.getY())	toCell = moveTopLeft(source);
		else if(source.getX()> destination.getX()&&	source.getY()< destination.getY())	toCell = moveTopRight(source);
		else if(source.getX()> destination.getX()&&	source.getY()==destination.getY())	toCell = moveUp(source);
		else if(source.getX()> destination.getX()&&	source.getY()> destination.getY())	toCell = moveTopLeft(source);
		else return list;

		System.out.println(getLocalName()+"getListOfCellsToMove: add move ("+toCell.getX()+","+toCell.getY()+")");
		list.add(0, toCell);
		return moveToCell(list, toCell, destination);
	}

	// This method creates an internal representation of a Map for determining next move.
	private Cell[][] generateMap(int height, int width) {
		Cell[][] mMap = new Cell[height+4][width+4];
		
		//center
		for(int row=2;row<mMap.length-2;row++){
			for(int col=2;col<mMap[row].length-2;col++) {
				if(row%2==col%2) {
					Cell cell = new Cell();
					cell.setX(row-1);
					cell.setY(col-1);
					mMap[row][col]=cell;						
				}
			}
		}
		
		//left
		for(int row=2;row<mMap.length-2;row++) {
			for(int col=0;col<2;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row][col+width];						
				}
			}
		}
		
		//right
		for(int row=2;row<mMap.length-2;row++) {
			for(int col=mMap[row].length-2;col<mMap[row].length;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row][col-width];						
				}
			}
		}
		
		//top
		for(int row=0;row<2;row++) {
			for(int col=2;col<mMap[row].length-2;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row+height][col];						
				}
			}
		}
		
		//bottom
		for(int row=mMap.length-2;row<mMap.length;row++) {
			for(int col=2;col<mMap[row].length-2;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row-height][col];						
				}
			}
		}
		
		//corner-top-left
		for(int row=0;row<2;row++) {
			for(int col=0;col<2;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row+height][col+width];						
				}
			}
		}
		
		//corner-top-right
		for(int row=0;row<2;row++) {
			for(int col=mMap[row].length-2;col<mMap[row].length;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row+height][col-width];						
				}
			}
		}
		
		//corner-bottom-left
		for(int row=mMap.length-2;row<mMap.length;row++) {
			for(int col=0;col<2;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row-height][col+width];						
				}
			}
		}
		
		//corner-bottom-right
		for(int row=mMap.length-2;row<mMap.length;row++) {
			for(int col=mMap[row].length-2;col<mMap[row].length;col++) {
				if(row%2==col%2) {
					mMap[row][col]=mMap[row-height][col-width];						
				}
			}
		}
		
		return mMap;
	}
	
	private Cell moveUp(Cell source) {
		return mMap[source.getX()-1][source.getY()+1];
	}
	
	private Cell moveTopRight(Cell source) {
		return mMap[source.getX()][source.getY()+2];
	}
	
	private Cell moveBottomRight(Cell source) {
		return mMap[source.getX()+2][source.getY()+2];
	}
	
	private Cell moveDown(Cell source) {
		return mMap[source.getX()+3][source.getY()+1];
	}
	
	private Cell moveBottomLeft(Cell source) {
		return mMap[source.getX()+2][source.getY()];
	}
	
	private Cell moveTopLeft(Cell source) {
		return mMap[source.getX()][source.getY()];
	}
	
	// ************************** //
	// *** BEHAVIOURS SECTION *** //
	// ************************** //
	
	private void movementRequest(Cell toCell) {
		moves.add(toCell);
	}

	private void cellAnalysisRequest(Cell cellToBeAnalyzed) {
		addBehaviour(new SimpleBehaviour(this)
		{
			private static final long serialVersionUID = 1L;
			boolean cellAnalysisRequestSent = false;
			AID terrainSimulatorAgentAID = new AID();

			@Override
			public void action() {
				
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(org.xploration.ontology.XplorationOntology.TERRAINSIMULATOR);
				dfd.addServices(sd);
				
				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[1];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0)
					{
						terrainSimulatorAgentAID = (AID) res[0].getName();
						// Send request for cell analysis to the terrainSimulator
						ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
						msg.addReceiver(terrainSimulatorAgentAID);
						msg.setLanguage(codec.getName());
						msg.setOntology(ontology.getName());

						msg.setProtocol(XplorationOntology.CELLANALYSIS);
						
						CellAnalysis cellAnalysis = new CellAnalysis();
						cellAnalysis.setCell(cellToBeAnalyzed);
						
						// As it is an action and the encoding language the SL, it must be wrapped into an Action
						Action agAction = new Action(terrainSimulatorAgentAID, cellAnalysis);
						try
						{
							// The ContentManager transforms the java objects into strings
							getContentManager().fillContent(msg, agAction);
							send(msg);
							System.out.println(getLocalName()+": Cell analysis for cell("+cellToBeAnalyzed.getX()+","+cellToBeAnalyzed.getY()+") request sent!");
							cellAnalysisRequestSent = true;
							
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public boolean done() 
			{
				return cellAnalysisRequestSent;
			}
			
		});
		
		// awaits for response of cell analysis request from platform simulator
		addBehaviour(new CyclicBehaviour(this)
		{
			private static final long serialVersionUID = 1L;
			public void action()
			{
				
				ACLMessage msg = receive(
						MessageTemplate.and(MessageTemplate.MatchProtocol(XplorationOntology.CELLANALYSIS),
						MessageTemplate.and(
						MessageTemplate.MatchLanguage(codec.getName()), 
						MessageTemplate.MatchOntology(ontology.getName()))));

				if (msg != null) {
					//System.out.println("MSG" + msg.getContent());
					try {
						if (msg.getPerformative() == ACLMessage.REFUSE) {
							// If what is received is REFUSE
							System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": "+msg.getProtocol()+" REFUSE --> Invalid cell coordinates.");
							
							// Change rover state to IDLE
							mRoverState = RoverState.IDLE;
							// Decide next move
							thinkRoverThink();
							
						} else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
							// If what is received is NOT UNDERSTOOD
							System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " "+msg.getProtocol()+" NOT UNDERSTOOD");
						} else if (msg.getPerformative() == ACLMessage.AGREE) {
							// If what is received is AGREE
							System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": "+msg.getProtocol()+" AGREE");
						} else if (msg.getPerformative() == ACLMessage.INFORM) {
							// If what is received is INFORM SUCCESS
							ContentElement ce = null;
							ce = getContentManager().extractContent(msg);
							if (ce instanceof Action) {
								Action agAction = (Action) ce;
								Concept conc = agAction.getAction();
								// If the action is CellAnalysis...
								if (conc instanceof CellAnalysis) {
									CellAnalysis cellAnalysis = (CellAnalysis) conc;
									Cell analyzedCell = new Cell();
									analyzedCell = cellAnalysis.getCell();
									
									System.out.println(myAgent.getLocalName()+": Cell successfully analyzed.\n          Cell info:\n          X coordinate: " + analyzedCell.getX() + "\n          Y coordinate: " + analyzedCell.getY() + "\n          Mineral:" + analyzedCell.getMineral());
									
									// Add newly analyzed cell to list of cells to claim
									mCellsToClaim.addCellList(analyzedCell);
									
									// Add newly analyzed cell to list of known cells
									mKnownCells.addCellList(analyzedCell);
									
									// Change rover state to IDLE
									mRoverState = RoverState.IDLE;
									// Decide next move
									thinkRoverThink();
								}
							}
							
						} else if (msg.getPerformative() == ACLMessage.FAILURE) {
							// if the analyzed cell is illegal. (cell is already occupied.)
							
							System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": " + msg.getContent());
							
							// Change rover state to IDLE
							mRoverState = RoverState.IDLE;
							// Decide next move
							thinkRoverThink();
							
						} else {
							System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " received but it is not REFUSE, AGREE nor NOT UNDERSTOOD!");
						}

					} catch (Exception e) {
						e.printStackTrace();
					} 
				} else {
					// If no message arrives
					block();
				}
			}
		});
	}
	
	private void sendMapBroadcastInform() {
		addBehaviour(new SimpleBehaviour(this)
		{
			private static final long serialVersionUID = 1L;
			boolean mapBroadcastSent = false;
			AID communicationSimulator = new AID();

			@Override
			public void action() {
				
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(XplorationOntology.MAPBROADCASTSERVICE);
				dfd.addServices(sd);
				
				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[1];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0)
					{
						communicationSimulator = (AID) res[0].getName();
						// Send request for cell analysis to the terrainSimulator
						ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
						msg.addReceiver(communicationSimulator);
						msg.setLanguage(codec.getName());
						msg.setOntology(ontology.getName());
						
						msg.setProtocol(XplorationOntology.MAPBROADCASTINFO);
						MapBroadcastInfo mapBroadcastInfo = new MapBroadcastInfo();
						mapBroadcastInfo.setMap(mKnownCells);
						
						// As it is an action and the encoding language the SL, it must be wrapped into an Action
						Action agAction = new Action(communicationSimulator, mapBroadcastInfo);
						try
						{
							// The ContentManager transforms the java objects into strings
							getContentManager().fillContent(msg, agAction);
							send(msg);
							System.out.println(getLocalName()+": Map Broadcast information sent!");
							mapBroadcastSent = true;
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public boolean done() 
			{
				return mapBroadcastSent;
			}
			
		});
		
		// Awaits for map broadcasts from communication simulator coming from other rovers in range
		addBehaviour(new CyclicBehaviour(this)
		{
			private static final long serialVersionUID = 1L;
			public void action()
			{
				
				ACLMessage msg = receive(
						MessageTemplate.and(MessageTemplate.MatchProtocol(XplorationOntology.MAPBROADCASTINFO),
						MessageTemplate.and(
						MessageTemplate.MatchLanguage(codec.getName()), 
						MessageTemplate.MatchOntology(ontology.getName()))));
				if (msg != null) {
					try {
						ContentElement ce = null;
						if (msg.getPerformative() == ACLMessage.INFORM) {
							ce = getContentManager().extractContent(msg);
							if (ce instanceof Action) {
								Action agAction = (Action) ce;
								Concept conc = agAction.getAction();
								if (conc instanceof MapBroadcastInfo) {
									MapBroadcastInfo mapBroadcast = (MapBroadcastInfo) conc;
									org.xploration.ontology.Map receivedListOfCells = mapBroadcast.getMap();
									System.out.println(myAgent.getLocalName()+": Map Broadcast Received from " + (msg.getSender()).getLocalName());
									
									@SuppressWarnings("unchecked")
									Iterator<Cell> iterator = receivedListOfCells.getAllCellList();
									while (iterator.hasNext()) {
										// Add received map broadcast to list of known cells (analyzed)
										Cell newCell = iterator.next();
										mKnownCells.addCellList(newCell);
										System.out.println("Received and added to list of analyzed cells this cell:\n");
										System.out.println("X: " + newCell.getX() + "\nY: " + newCell.getY() + "\nMineral: " + newCell.getMineral());
									}
								}
							}
						} 
					} catch (Exception e) {
						e.printStackTrace();
					} 
				} else {
					// If no message arrives
					block();
				}
			}
		});
	}
	
	private void claimCells(org.xploration.ontology.Map mapOfCellsToClaim) {		
		addBehaviour(new SimpleBehaviour(this)
		{
			private static final long serialVersionUID = 1L;
			boolean claimCellSent = false;
			AID communicationSimulator = new AID();

			@Override
			public void action() {
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(XplorationOntology.RADIOCLAIMSERVICE);
				dfd.addServices(sd);
				
				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[1];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0) {
						communicationSimulator = (AID) res[0].getName();
						// Send request for claim cell INFORM to communication simulator
						ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
						msg.addReceiver(communicationSimulator);
						msg.setLanguage(codec.getName());
						msg.setOntology(ontology.getName());
						
						msg.setProtocol(XplorationOntology.CLAIMCELLINFO);
						
						ClaimCellInfo claimCellInfo = new ClaimCellInfo();
						claimCellInfo.setMap(mapOfCellsToClaim);
						Team team = new Team();
						team.setTeamId(CompanyConstants.MY_TEAM_ID);
						claimCellInfo.setTeam(team);
						
						Action agAction = new Action(communicationSimulator, claimCellInfo);
						try {
							// The ContentManager transforms the java objects into strings
							getContentManager().fillContent(msg, agAction);
							send(msg);
							System.out.println(getLocalName()+": CLAIMCELLINFO sent to network!");
							claimCellSent = true;
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public boolean done() {
				return claimCellSent;
			}
		});
	}
	
	class IssueMovement extends CyclicBehaviour {
		
		public IssueMovement(Agent a) {
			super(a);
		}
		
		private static final long serialVersionUID = 1L;
		
		AID movementSimulator = new AID();

		@Override
		public void action() {
			
			// Creates the description for the type of agent to be searched
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType(org.xploration.ontology.XplorationOntology.MOVEMENTREQUESTSERVICE);
			dfd.addServices(sd);
			
			try {
				// It finds agents of the required type
				DFAgentDescription[] res = new DFAgentDescription[1];
				res = DFService.search(myAgent, dfd);

				// Gets the first occurrence, if there was success
				if (res.length > 0 && !mRoverState.equals(RoverState.MOVING) && moves.size()!=0)
				{
					movementSimulator = (AID) res[0].getName();
					// Send request for cell analysis to the platform simulator
					ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
					msg.addReceiver(movementSimulator);
					msg.setLanguage(codec.getName());
					msg.setOntology(ontology.getName());
					
					msg.setProtocol(org.xploration.ontology.XplorationOntology.MOVEMENTREQUESTINFO);
					
					MovementRequestInfo movementRequestInfo = new MovementRequestInfo();
					Cell toCell = moves.peek();
					movementRequestInfo.setCell(toCell);
					
					// As it is an action and the encoding language the SL, it must be wrapped into an Action
					Action agAction = new Action(movementSimulator, movementRequestInfo);
					try
					{
						// The ContentManager transforms the java objects into strings
						getContentManager().fillContent(msg, agAction);
						send(msg);
						System.out.println(getLocalName()+": Movement request for cell("+ toCell.getX()+","+ toCell.getY()+") " + " sent!");
						mCellToMoveTo = toCell;
						mRoverState = RoverState.MOVING;
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	class WaitForMovementResponse extends CyclicBehaviour {
		
		public WaitForMovementResponse(Agent a) {
			super(a);
		}
		private static final long serialVersionUID = 1L;
		public void action()
		{
			ACLMessage msg = receive(
					MessageTemplate.and(MessageTemplate.MatchProtocol(XplorationOntology.MOVEMENTREQUESTINFO),
					MessageTemplate.and(
					MessageTemplate.MatchLanguage(codec.getName()), 
					MessageTemplate.MatchOntology(ontology.getName()))));
			if (msg != null) {
				//System.out.println("MSG" + msg.getContent());
				try {
					if (msg.getPerformative() == ACLMessage.REFUSE) {
						// If what is received is REFUSE
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": "+msg.getProtocol()+" REFUSE --> "+msg.getContent());
						
						// Change rover state to IDLE
						mRoverState = RoverState.IDLE;
						// Decide next move
						thinkRoverThink();
						
					} else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
						// If what is received is NOT UNDERSTOOD
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " "+msg.getProtocol()+" NOT UNDERSTOOD");
					} else if (msg.getPerformative() == ACLMessage.AGREE) {
						// If what is received is AGREE
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": "+msg.getProtocol()+" AGREE");
					} else if (msg.getPerformative() == ACLMessage.INFORM) {
						// If what is received is INFORM SUCCESS
						
						mRoverPosition.setX(mCellToMoveTo.getX());
						mRoverPosition.setY(mCellToMoveTo.getY());
						System.out.println(myAgent.getLocalName()+": Successfully moved to ("+mRoverPosition.getX()+","+mRoverPosition.getY()+")." + "\n           *************" + "\n           *************" + "\n           *************" + "\n           *************");
						
						moves.poll();
						// Change rover state to IDLE
						mRoverState = RoverState.IDLE;
						
						// After each successful movement send a map broadcast inform to nearby rovers
						sendMapBroadcastInform();
						
						// Decide next move
						thinkRoverThink();
						
					} else if (msg.getPerformative() == ACLMessage.FAILURE) {
						// if illegal movement
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + ": " + msg.getContent());
						
						// Change rover state to IDLE
						mRoverState = RoverState.IDLE;
						// Decide next move
						thinkRoverThink();
						
					} else {
						System.out.println(myAgent.getLocalName()+": Message from "+ (msg.getSender()).getLocalName() + " received but it is not REFUSE, AGREE nor NOT UNDERSTOOD!");
					}

				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				// If no message arrives
				block();
			}
		}
		
	}
	
    class SendRoverRegistrationBehaviour extends SimpleBehaviour {

    	public SendRoverRegistrationBehaviour(Agent a) { 
            super(a);  
        }
    	private static final long serialVersionUID = 1L;
    	boolean roverRegistrationSent = false;
		AID platformSimulator = new AID();

		@Override
		public void action() {
			
			// Creates the description for the type of agent to be searched
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType(org.xploration.ontology.XplorationOntology.ROVERREGISTRATIONSERVICE);
			dfd.addServices(sd);
			
			try {
				// It finds agents of the required type
				DFAgentDescription[] res = new DFAgentDescription[1];
				res = DFService.search(myAgent, dfd);
				// Gets the first occurrence, if there was success
				if (res.length > 0)
				{
					platformSimulator = (AID) res[0].getName();

					RoverRegistrationInfo roverRegistrationInfo = new RoverRegistrationInfo();
					Team myTeam = new Team();
					myTeam.setTeamId(CompanyConstants.MY_TEAM_ID);
					roverRegistrationInfo.setTeam(myTeam);
					roverRegistrationInfo.setCell(mRoverPosition);
					
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.addReceiver(platformSimulator);
					msg.setLanguage(codec.getName());
					msg.setOntology(ontology.getName());
					
					msg.setProtocol(XplorationOntology.ROVERREGISTRATIONINFO);
					
					// As it is an action and the encoding language the SL, it must be wrapped into an Action
					Action agAction = new Action(platformSimulator, roverRegistrationInfo);
					try {
						// The ContentManager transforms the java objects into strings
						contentManager.fillContent(msg, agAction);
						send(msg);
						System.out.println(getLocalName()+": Rover registration sent!");
						roverRegistrationSent = true;
					}
					catch (CodecException ce) {
						ce.printStackTrace();
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean done() {
			return roverRegistrationSent;
		}
    }
    
	private Behaviour missionLengthCountdown() {
		int missionLengthInMiliseconds = mMissionLength * 1000;
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				// Terminate Agent Rover
				myAgent.doDelete();
				System.out.println(myAgent.getLocalName()+": terminated himself!");
			 }
		};
	}
	
	private Behaviour claimBufferSize2() {
		int divider = 2;
		int missionLengthInMiliseconds = (mMissionLength * 1000) - ((mMissionLength * 1000) / divider);
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				mClaimBufferSize = 3;
				System.out.println(myAgent.getLocalName()+": Claim buffer size = " + mClaimBufferSize);
			 }
		};
	}
	
	private Behaviour claimBufferSize4() {
		int divider = 4;
		int missionLengthInMiliseconds = (mMissionLength * 1000) - ((mMissionLength * 1000) / divider);
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				mClaimBufferSize = 2;
				System.out.println(myAgent.getLocalName()+": Claim buffer size = " + mClaimBufferSize);
			 }
		};
	}
	
	private Behaviour claimBufferSize8() {
		int divider = 8;
		int missionLengthInMiliseconds = (mMissionLength * 1000) - ((mMissionLength * 1000) / divider);
		// After mission length expires kill company, capsule and rover agents
		return new WakerBehaviour(this, missionLengthInMiliseconds) {
			private static final long serialVersionUID = 1L;

			protected void onWake() {
				mClaimBufferSize = 1;
				System.out.println(myAgent.getLocalName()+": Claim buffer size = " + mClaimBufferSize);
			 }
		};
	}
	
	// This method returns a list of surrounding cells for a given level (moving clockwise) 
	// Note: if level is 0, it returns the source cell.
	private ArrayList<Cell> getSurroundingCells(Cell source, int level) {
		
		ArrayList<Cell> surroundingCells = new ArrayList<Cell>();
		Cell currentCell = source;
		
		for(int i=0;i<level;i++) {
			currentCell = moveUp(currentCell);
		}
		surroundingCells.add(currentCell);
		
		for(int i=0;i<level;i++) {
			currentCell = moveBottomRight(currentCell);
			surroundingCells.add(currentCell);
		}
		
		for(int i=0;i<level;i++) {
			currentCell = moveDown(currentCell);
			surroundingCells.add(currentCell);
		}
		
		for(int i=0;i<level;i++) {
			currentCell = moveBottomLeft(currentCell);
			surroundingCells.add(currentCell);
		}
		
		for(int i=0;i<level;i++) {
			currentCell = moveTopLeft(currentCell);
			surroundingCells.add(currentCell);
		}
		
		for(int i=0;i<level;i++) {
			currentCell = moveUp(currentCell);
			surroundingCells.add(currentCell);
		}
		
		for(int i=0;i<level-1;i++) {
			currentCell = moveTopRight(currentCell);
			surroundingCells.add(currentCell);
		}
		
		//remove duplicates
		Set<Cell> surroundingCellsWithoutDuplicates = new LinkedHashSet<Cell>(surroundingCells);
		surroundingCells.clear();
		surroundingCells.addAll(surroundingCellsWithoutDuplicates);
		
		//remove already visited cells
		ArrayList<Cell> unvisitedSurroundingCells = new ArrayList<>();
		for(int i=0;i<surroundingCells.size();i++){ 
			boolean found = false;
			@SuppressWarnings("unchecked")
			Iterator<Cell> iterator = mKnownCells.getAllCellList();
			while (!found&&iterator.hasNext()) {
				Cell cell = iterator.next();
				if(isEqualXY(surroundingCells.get(i),cell)) {
					found = true;
				}
			}
			if(!found) unvisitedSurroundingCells.add(surroundingCells.get(i));
		}

		System.out.print(getLocalName()+": visited cells: Level="+level+" :");
		@SuppressWarnings("unchecked")
		Iterator<Cell> iterator = mKnownCells.getAllCellList();
		while (iterator.hasNext()) {
			Cell cell = iterator.next();
        	System.out.print("("+cell.getX()+","+cell.getY()+")");
		}
        System.out.println();
		System.out.print(getLocalName()+": getSurroundingCells: Level="+level+" :");
        for(int i=0;i<unvisitedSurroundingCells.size();i++)
        	System.out.print("("+unvisitedSurroundingCells.get(i).getX()+","+unvisitedSurroundingCells.get(i).getY()+")");
        System.out.println();
        
		return unvisitedSurroundingCells;
	}
	
	private boolean isWithinRange(Cell broadcastLocation, Cell receiverLocation) {
			
		// N is Height
		// M is Width
		
		int x = broadcastLocation.getX();
		int y = broadcastLocation.getY();
		int recX = receiverLocation.getX();
		int recY = receiverLocation.getY();
		
		//vertical down
		for(int i = 1; i<=mRadioRange; i++){
			int cellX = x+(i*2);
			if(cellX > N) cellX = cellX - N;
			
			if(recX == cellX && recY == y) {
				return true;
			}
		}
		//vertical up 
		for (int i = 1; i<=mRadioRange; i++){
			int cellX = x-(i*2);
			if (cellX <= 0) cellX = N + cellX;
			
			if(recX == cellX && recY == y) {
				return true;
			}
		}
		
		//diagonal left up
		for(int i = 1; i<=mRadioRange; i++){
			int cellX = x-i*1;
			int cellY =  y-i*1;
			if(cellX <= 0) cellX = N + cellX;
			if(cellY <= 0 ) cellY = M + cellY;
			
			if(recX == cellX && recY == cellY) {
				return true;
			}
		}
		
		//diagonal right up 
		for(int i = 1; i<=mRadioRange;i++){
			int cellX = x-i*1;
			int cellY = y+i*1;
			if(cellX <= 0) cellX = N + cellX;
			if(cellY > M) cellY = cellY - M;
		
			if(recX ==cellX  && recY == cellY ) {
				return true;
			}
		}
		
		//diagonal left down 
		for(int i = 1; i<=mRadioRange;i++){
			int cellX = x+i*1;
			int cellY = y-i*1;
			if(cellX > N) cellX = cellX - N;
			if(cellY <= 0 ) cellY = M + cellY;
			
			if(recX == cellX && recY == cellY) {
				return true;
			}
		}
		
		//diagonal right down 
		for(int i = 1; i<=mRadioRange;i++){
			int cellX = x+i*1;
			int cellY =  y+i*1;
			if(cellX > N) cellX = cellX - N;
			if(cellY > M) cellY = cellY - M;
			
			if(recX ==cellX  && recY == cellY) {
				return true;
			}
		}
		return false;
	}

}
